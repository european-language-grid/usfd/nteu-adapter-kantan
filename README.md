# ELG Adapter for NTEU engines from KantanAI

This is a simple "adapter" which accepts requests in the [ELG](https://www.european-language-grid) API format and translates them to the API format required by translation engines produced by [KantanAI](https://www.kantanai.io) for the [NTEU](https://nteu.eu) project.  The adapter can run alongside any KantanAI NTEU engine to allow it to be run within the ELG infrastructure.

The KantanAI translation engine has a simple API which expects an `application/x-www-form-urlencoded` POST request with parameters `str` (the string to translate), `src` (the source language code) and `trg` (the target language code).  When running in the ELG infrastructure the adapter is able to deduce the correct src and trg values from its runtime environment, for use elsewhere they must be specified explicitly via `SRC_LANG` and `TRG_LANG` environment variables.
