#
# Adapter to map the native API of NTEU translation engines from Kantan AI into
# the format required for integration into the European Language Grid.
#
# (c) 2022 European Language Grid
#
# This software is released under the GNU General Public Licence version 3.0,
# see the LICENSE file for details.
#

from elg import QuartService
from elg.quart_service import ProcessingError
from elg.model import TextsResponse, TextsResponseObject
import aiohttp
import traceback
import os
import asyncio
import re
from loguru import logger

ENGINE_ENDPOINT = os.environ.get("ENGINE_ENDPOINT", "https://localhost:8000")
SRC = os.environ.get("SRC_LANG")
TRG = os.environ.get("TRG_LANG")

# If no source and target languages specified explicitly, attempt to infer them
# from the name of the knative service (which will include "nteu-{src}-{trg}")
if not(SRC) or not(TRG):
    m = re.search(r'nteu-([a-z]+)-([a-z]+)', os.environ.get("K_SERVICE", ""))
    if m:
        (SRC, TRG) = m.groups()

class NTEUAdapterKantan(QuartService):

    async def wait_for_success(self, component, make_request):
        logger.info(f"Waiting for {component} to become ready")
        tries = 30
        while tries > 0:
            tries -= 1
            try:
                async with make_request(self.session) as client_response:
                    resp_text = await client_response.text()
                    resp_len = len(resp_text)
                    if client_response.ok:
                        logger.info(f"Request to {component} succeeded: {resp_len} characters in response")
                        if resp_len < 100:
                            logger.info(f"Response was: {resp_text}")
                        return
                    else:
                        logger.info(f"Request to {component} failed: {resp_len} characters in response")
            except Exception as ex:
                logger.info(f"Request to {component} failed with an exception: {ex}")
            await asyncio.sleep(2)
        raise RuntimeError(f"{component} did not become ready")


    async def setup(self):
        # Backend is https with a self-signed certificate, so disable verification
        self.session = aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False))
        # Ensure all dependencies are up before we start listening
        try:
            await self.wait_for_success("engine", lambda s: s.post(ENGINE_ENDPOINT, data={'str': 'Test', 'src': SRC, 'trg': TRG}))
        except:
            os._exit(1)

    async def shutdown(self):
        if self.session is not None:
            await self.session.close()

    async def process_text(self, request):
        reply_text = await self.call_engine(request.content)
        return TextsResponse(texts=[TextsResponseObject(content=reply_text, role="segment")])

    async def call_engine(self, text):
        try:
            # Make the remote call
            async with self.session.post(ENGINE_ENDPOINT, data={'str': text, 'src': SRC, 'trg': TRG}) as client_response:
                status_code = client_response.status
                if status_code >= 400:
                    raise ProcessingError.InternalError(await client_response.text())
                return await client_response.text()
        except:
            traceback.print_exc()
            raise ProcessingError.InternalError('Error calling API')


service = NTEUAdapterKantan("NTEUAdapter")
app = service.app
